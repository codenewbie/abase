//
// DO NOT EDIT THIS FILE, IT HAS BEEN GENERATED USING AndroidAnnotations 3.0.1.
//


package com.jayqqaa12.abase.core;

import android.content.Context;

public final class ADao_
    extends ADao
{

    private Context context_;

    private ADao_(Context context) {
        context_ = context;
        init_();
    }

    public static ADao_ getInstance_(Context context) {
        return new ADao_(context);
    }

    private void init_() {
    }

    public void rebind(Context context) {
        context_ = context;
        init_();
    }

}
